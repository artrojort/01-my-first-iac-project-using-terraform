terraform destroy -target=aws_instance.instance-web -target=aws_security_group.arturo-sg

terraform state list

terraform apply --auto-approve

terraform plan

terraform validate

terraform state show 

AWS user_data log: /var/log/cloud-init-output.log

AWS user_data launch: /var/lib/cloud/instance

terraform plan -var instance_type="t2.micro"