variable "instance_type" {
  type = string
  default = "t2.micro"
}


resource "aws_security_group" "arturo-sg" {
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Apache http port"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  name = "arturo-sg"
}

resource "aws_key_pair" "arturorojas_oregon_key" {
  key_name   = "arturokey"
  public_key = file("~/.ssh/id_rsa.pub")
}

data "aws_ami" "amazon_ami" {
  most_recent = true
  filter {
    name = "name"
    values = ["amzn2-ami-ecs-hvm-2.0.20190603-x86_64-ebs"]
  }
  owners = ["591542846629"]
}

resource "aws_instance" "web" {
    ami = data.aws_ami.amazon_ami.id
    instance_type = var.instance_type

    vpc_security_group_ids = [aws_security_group.arturo-sg.id]
    tags = {
        Name = "Arturo-Instance"
    }

    key_name = "arturo.rojas"

    user_data = file("./user_data.txt")
}
output "security_group_id" {
  value = aws_security_group.arturo-sg.id
}


